package com.restaurante.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.restaurante.entity.PratoEntity;
import com.restaurante.service.PratoService;
import com.restaurante.service.PratoService.PratoPages;

@RestController
@RequestMapping("prato")
public class PratoController {
	
	@Autowired PratoService pratoService;
	
//	@RequestMapping(value = "/all", method = RequestMethod.GET)
//	public List<PratoEntity> buscaTodosPratos() {
//		return pratoService.;
//	}
	
	@CrossOrigin
	@RequestMapping(value = "/id={idPrato}", method = RequestMethod.GET)
	public PratoEntity buscarPratoPorId(@PathVariable Integer idPrato) {
		
		return pratoService.buscarPratoPorId(idPrato);
	}
	
//	@RequestMapping(value = "/descricao={descricao}", method = RequestMethod.GET)
//	public List<PratoEntity> buscaPratoPorDescricao(@PathVariable String descricao) {
//		return new ArrayList<PratoEntity>();
//	}
	
	@CrossOrigin
	@RequestMapping(value = "/descricao={descricao}/pagina={pagina}/itensPorPagina={itensPorPagina}", method = RequestMethod.GET)
	public PratoPages buscaPratoPorDescricaoPaginado(@PathVariable String descricao, @PathVariable Integer pagina, @PathVariable Integer itensPorPagina) {
		
		return pratoService.buscarPratoPorDescricaoPaginado(descricao, pagina, itensPorPagina);
	}
	
	@CrossOrigin
	@RequestMapping(value = "/id={idPrato}", method = RequestMethod.DELETE)
	public void excluirPratoPorId(@PathVariable Integer idPrato) {
		pratoService.excluirPrato(idPrato);
	}
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST)
	public PratoEntity cadastrarPrato(@RequestBody PratoJson pratoJson) {
		
		return pratoService.salvarPrato(pratoJson);
	}
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.PUT)
	public PratoEntity editarPrato(@RequestBody PratoJson pratoJson) {
		
		return pratoService.salvarPrato(pratoJson);
	}
	
	public static class PratoJson {
		public Integer idPrato;
		public Integer idRestaurante;
		public String descricao;
		public Float preco;
	
	
		@JsonCreator
		public PratoJson(
				@JsonProperty("idPrato") Integer idPrato,
				@JsonProperty("idRestaurante") Integer idRestaurante,
				@JsonProperty("descricao") String descricao,
				@JsonProperty("preco") Float preco
				) {
			this.idPrato = idPrato;
			this.idRestaurante = idRestaurante;
			this.descricao = descricao;
			this.preco = preco;
			
			
		}
	
	
		public Integer getIdPrato() {
			return idPrato;
		}
	
		public void setIdPrato(Integer idPrato) {
			this.idPrato = idPrato;
		}
	
		public Integer getIdRestaurante() {
			return idRestaurante;
		}
	
		public void setIdRestaurante(Integer idRestaurante) {
			this.idRestaurante = idRestaurante;
		}
	
	
		public String getDescricao() {
			return descricao;
		}
	
		public void setDescricao(String descricao) {
			this.descricao = descricao;
		}
	
		public Float getPreco() {
			return preco;
		}
	
		public void setPreco(Float preco) {
			this.preco = preco;
		}
	}

}
