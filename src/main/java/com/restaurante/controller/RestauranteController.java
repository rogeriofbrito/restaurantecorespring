package com.restaurante.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.restaurante.entity.RestauranteEntity;
import com.restaurante.service.RestauranteService;
import com.restaurante.service.RestauranteService.RestaurantePages;

@RestController
@RequestMapping("restaurante")
public class RestauranteController {
	
	@Autowired RestauranteService restauranteService;
	
	@CrossOrigin
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public List<RestauranteEntity> buscaTodosRestaurantes() {
		
		return restauranteService.getAll();
	}

	@CrossOrigin
	@RequestMapping(value = "/id={idRestaurante}", method = RequestMethod.GET)
	public RestauranteEntity buscarRestaurantePorId(@PathVariable Integer idRestaurante) {
		
		return restauranteService.buscarRestaurantePorId(idRestaurante);
	}

//	@RequestMapping(value = "/descricao={descricao}", method = RequestMethod.GET)
//	public List<RestauranteEntity> buscaRestaurantePorDescricaoPaginado(@PathVariable String descricao) {
//		
//		return restauranteService.buscarRestaurantePorDescricao(descricao);
//	}
	
	@CrossOrigin
	@RequestMapping(value = "/descricao={descricao}/pagina={pagina}/itensPorPagina={itensPorPagina}", method = RequestMethod.GET)
	public RestaurantePages buscaRestaurantePorDescricaoPaginado(@PathVariable String descricao, @PathVariable Integer pagina, @PathVariable Integer itensPorPagina) {
		
		return restauranteService.buscarRestaurantePorDescricaoPaginado(descricao, pagina, itensPorPagina);
	}
	
	@CrossOrigin
	@RequestMapping(value = "/id={idRestaurante}", method = RequestMethod.DELETE)
	public void excluirRestaurantePorId(@PathVariable Integer idRestaurante) {
		restauranteService.excluirRestaurante(idRestaurante);
	}
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.POST)
	public RestauranteEntity cadastrarRestaurante(@RequestBody RestauranteEntity restauranteEntity) {
		
		return restauranteService.salvarRestaurante(restauranteEntity);
	}
	
	@CrossOrigin
	@RequestMapping(method = RequestMethod.PUT)
	public RestauranteEntity editarRestaurante(@RequestBody RestauranteEntity restauranteEntity) {
		
		return restauranteService.salvarRestaurante(restauranteEntity);
	}
	
//	public static class RestauranteJson {
//		public Integer idRestaurante;
//		public String descricao;
//
//		@JsonCreator
//		public RestauranteJson(
//				@JsonProperty("idRestaurante") Integer idRestaurante,
//				@JsonProperty("descricao") String descricao
//				) {
//			this.idRestaurante = idRestaurante;
//			this.descricao = descricao;
//		}
//	}
}
