package com.restaurante.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.restaurante.entity.RestauranteEntity;

@Repository
public interface RestauranteRepository extends CrudRepository<RestauranteEntity, Integer> {
	//List<RestauranteEntity> findByDescricaoContaining(String descricao);
	
	List<RestauranteEntity> findByDescricaoContainingIgnoreCase(String descricao, Pageable pageable);
	
	Integer countByDescricaoContaining(String descricao);
}
