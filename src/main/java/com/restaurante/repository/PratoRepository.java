package com.restaurante.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.restaurante.entity.PratoEntity;

@Repository
public interface PratoRepository extends CrudRepository<PratoEntity, Integer> {
	
	List<PratoEntity> findByDescricaoContainingIgnoreCase(String descricao, Pageable pageable);
	
	Integer countByDescricaoContaining(String descricao);
}
