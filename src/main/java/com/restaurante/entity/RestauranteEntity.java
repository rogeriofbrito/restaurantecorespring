package com.restaurante.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "restaurante")
public class RestauranteEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
    @SequenceGenerator(name="restauranteSequence", sequenceName="restaurante_idRestaurante_seq", allocationSize=1, initialValue=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "idrestaurante", insertable = false)
	private Integer idRestaurante;
	
	@Column(name = "descricao")
	private String descricao;


	public Integer getIdRestaurante() {
		return idRestaurante;
	}


	public void setIdRestaurante(Integer idRestaurante) {
		this.idRestaurante = idRestaurante;
	}


	public String getDescricao() {
		return descricao;
	}


	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
