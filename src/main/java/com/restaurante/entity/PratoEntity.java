package com.restaurante.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "prato")
public class PratoEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
    @SequenceGenerator(name="pratoSequence", sequenceName="prato_idPrato_seq", allocationSize=1, initialValue=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "idprato", insertable = false)
	private Integer idPrato;
	
    @OneToOne
    @JoinColumns(value = {
            @JoinColumn(name = "idrestaurante", referencedColumnName = "idrestaurante")
    })
	private RestauranteEntity restaurante;
	
    @Column(name = "descricao")
	private String descricao;
	
    @Column(name = "preco")
	private Float preco;

	public Integer getIdPrato() {
		return idPrato;
	}

	public void setIdPrato(Integer idPrato) {
		this.idPrato = idPrato;
	}

	public RestauranteEntity getRestaurante() {
		return restaurante;
	}

	public void setRestaurante(RestauranteEntity restaurante) {
		this.restaurante = restaurante;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Float getPreco() {
		return preco;
	}

	public void setPreco(Float preco) {
		this.preco = preco;
	}
}
