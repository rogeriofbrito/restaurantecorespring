package com.restaurante.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

//@SpringBootApplication
@SpringBootConfiguration //para funcionar testes
@Configuration
@EnableAutoConfiguration
@ComponentScan({ "com.restaurante.controller", "com.restaurante.service"} )
@EntityScan(basePackages = { "com.restaurante.entity" })
@EnableJpaRepositories(basePackages = { "com.restaurante.repository" })

public class RestauranteCoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestauranteCoreApplication.class, args);
	}
}
