package com.restaurante.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.restaurante.controller.PratoController.PratoJson;
import com.restaurante.entity.PratoEntity;
import com.restaurante.repository.PratoRepository;
import com.restaurante.repository.RestauranteRepository;

@Service
public class PratoService {
	
	@Autowired PratoRepository pratoRepository;
	@Autowired RestauranteRepository restauranteRepository;
	
	public PratoEntity salvarPrato(PratoJson pratoJson) {
		PratoEntity pratoEntity = new PratoEntity();
		pratoEntity.setIdPrato(pratoJson.getIdPrato());
		pratoEntity.setRestaurante(restauranteRepository.findOne(pratoJson.getIdRestaurante()));
		pratoEntity.setDescricao(pratoJson.getDescricao());
		pratoEntity.setPreco(pratoJson.getPreco());
		return pratoRepository.save(pratoEntity);
	}
	
	public PratoEntity buscarPratoPorId(Integer idPrato) {
		return pratoRepository.findOne(idPrato);
	}
	
	public PratoPages buscarPratoPorDescricaoPaginado(String descricao, Integer pagina, Integer itensPorPagina) {
		Pageable pageable = new PageRequest(pagina, itensPorPagina);
		PratoPages pratoPages = new PratoPages();
		
		pratoPages.setListPratoEntity(pratoRepository.findByDescricaoContainingIgnoreCase(descricao, pageable));
		pratoPages.setCount(pratoRepository.countByDescricaoContaining(descricao));

		return pratoPages;
	}
	
	public void excluirPrato(Integer idPrato) {
		
		pratoRepository.delete(pratoRepository.findOne(idPrato));
	}
	
	public static class PratoPages {
		@JsonProperty("pratos") private List<PratoEntity> listPratoEntity;
		@JsonProperty("total") private Integer count;
		
		public List<PratoEntity> getListPratoEntity() {
			return listPratoEntity;
		}
		public void setListPratoEntity(List<PratoEntity> listPratoEntity) {
			this.listPratoEntity = listPratoEntity;
		}
		public Integer getCount() {
			return count;
		}
		public void setCount(Integer count) {
			this.count = count;
		}
	}
}
