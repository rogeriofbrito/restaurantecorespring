package com.restaurante.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.restaurante.entity.RestauranteEntity;
import com.restaurante.repository.RestauranteRepository;

@Service
public class RestauranteService {
	
	@Autowired RestauranteRepository restauranteRepository;

	public RestauranteEntity salvarRestaurante(RestauranteEntity restauranteEntity) {	
		return restauranteRepository.save(restauranteEntity);
	}
	
	public List<RestauranteEntity> getAll() {
		List<RestauranteEntity> listRestaurante = new ArrayList<>();
		Iterable<RestauranteEntity> iterableRestaurante = restauranteRepository.findAll();
		iterableRestaurante.forEach(listRestaurante::add);
		
		return listRestaurante;
	}
	
	public RestauranteEntity buscarRestaurantePorId(Integer idRestaurante) {
		return restauranteRepository.findOne(idRestaurante);
	}
	
//	public List<RestauranteEntity> buscarRestaurantePorDescricao(String descricao) {
//		return restauranteRepository.findByDescricaoContaining(descricao);
//	}
	
	public RestaurantePages buscarRestaurantePorDescricaoPaginado(String descricao, Integer pagina, Integer itensPorPagina) {
		Pageable pageable = new PageRequest(pagina, itensPorPagina);
		RestaurantePages restaurantePages = new RestaurantePages();
		
		restaurantePages.setListRestauranteEntity(restauranteRepository.findByDescricaoContainingIgnoreCase(descricao, pageable));
		restaurantePages.setCount(restauranteRepository.countByDescricaoContaining(descricao));

		return restaurantePages;
	}
	
	public void excluirRestaurante(Integer idRestaurante) {
		
		restauranteRepository.delete(restauranteRepository.findOne(idRestaurante));
	}
	
	public static class RestaurantePages {
		@JsonProperty("restaurantes") private List<RestauranteEntity> listRestauranteEntity;
		@JsonProperty("total") private Integer count;
		
		public List<RestauranteEntity> getListRestauranteEntity() {
			return listRestauranteEntity;
		}
		
		public void setListRestauranteEntity(List<RestauranteEntity> listRestauranteEntity) {
			this.listRestauranteEntity = listRestauranteEntity;
		}
		
		public Integer getCount() {
			return count;
		}
		
		public void setCount(Integer count) {
			this.count = count;
		}
	}
}
